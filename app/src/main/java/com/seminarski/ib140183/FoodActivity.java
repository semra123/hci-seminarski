package com.seminarski.ib140183;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.seminarski.ib140183.Model.Food;

public class FoodActivity extends AppCompatActivity implements View.OnClickListener {

    Button btn_item1,btn_item2,btn_item3;
    ImageView iv_item1,iv_item2,iv_item3;
    TextView tv_item1_name,tv_item1_desc,tv_item2_name,tv_item2_desc,tv_item3_name,tv_item3_desc;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_food);

        iv_item1 = (ImageView)findViewById(R.id.iv_item1);
        iv_item2 = (ImageView)findViewById(R.id.iv_item2);
        iv_item3 = (ImageView)findViewById(R.id.iv_item3);

        tv_item1_name = (TextView)findViewById(R.id.tv_item1_name);
        tv_item2_name = (TextView)findViewById(R.id.tv_item2_name);
        tv_item3_name = (TextView)findViewById(R.id.tv_item3_name);

        tv_item1_desc = (TextView)findViewById(R.id.tv_item1_desc);
        tv_item2_desc = (TextView)findViewById(R.id.tv_item2_desc);
        tv_item3_desc = (TextView)findViewById(R.id.tv_item3_desc);

        btn_item1 = (Button)findViewById(R.id.btn_item1);
        btn_item2 = (Button)findViewById(R.id.btn_item2);
        btn_item3 = (Button)findViewById(R.id.btn_item3);
        btn_item1.setOnClickListener(this);
        btn_item2.setOnClickListener(this);
        btn_item3.setOnClickListener(this);

        Intent intent = getIntent();

        boolean isSweet = intent.getBooleanExtra("isSweet",false);
        boolean isDrinks = intent.getBooleanExtra("isDrink",false);
        if(isSweet){
            setViewforSweet();
        }
        if(isDrinks){
            setViewforDrinks();
        }









    }

    @Override
    public void onClick(View v) {
        FirebaseDatabase database = FirebaseDatabase.getInstance();

        DatabaseReference myRef = database.getReference("Food");

        if(v.getId()==R.id.btn_item1){
            final Intent intent = new Intent(FoodActivity.this,FoodDetailActivity.class);
            Intent intent1= getIntent();
            boolean isSweet = intent1.getBooleanExtra("isSweet",false);
            boolean isSlat = intent1.getBooleanExtra("isSlat",false);
            boolean isDrinks = intent1.getBooleanExtra("isDrink",false);


            if(isSweet){

                myRef.child("4").addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        Food food = dataSnapshot.getValue(Food.class);

                        intent.putExtra("id",4);
                        intent.putExtra("name",food.getName());
                        intent.putExtra("desc",food.getDesc());
                        intent.putExtra("price_large",food.getPrice_large());
                        intent.putExtra("price_small",food.getPrice_small());


                        startActivity(intent);
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
            }
            if(isDrinks){
            myRef.child("7").addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    Food food = dataSnapshot.getValue(Food.class);

                    intent.putExtra("id",7);
                    intent.putExtra("name",food.getName());
                    intent.putExtra("desc",food.getDesc());
                    intent.putExtra("price_large",food.getPrice_large());
                    intent.putExtra("price_small",food.getPrice_small());


                    startActivity(intent);
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });
            }

            if(isSlat){
            myRef.child("1").addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    Food food = dataSnapshot.getValue(Food.class);

                    intent.putExtra("id",1);
                    intent.putExtra("name",food.getName());
                    intent.putExtra("desc",food.getDesc());
                    intent.putExtra("price_large",food.getPrice_large());
                    intent.putExtra("price_small",food.getPrice_small());


                    startActivity(intent);
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });
            }





        }

        else if(v.getId()==R.id.btn_item2){

            final Intent intent = new Intent(FoodActivity.this,FoodDetailActivity.class);
            Intent intent1= getIntent();
            boolean isSweet = intent1.getBooleanExtra("isSweet",false);
            boolean isSlat = intent1.getBooleanExtra("isSlat",false);
            boolean isDrinks = intent1.getBooleanExtra("isDrink",false);


            if(isSweet){

                myRef.child("5").addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        Food food = dataSnapshot.getValue(Food.class);

                        intent.putExtra("id",5);
                        intent.putExtra("name",food.getName());
                        intent.putExtra("desc",food.getDesc());
                        intent.putExtra("price_large",food.getPrice_large());
                        intent.putExtra("price_small",food.getPrice_small());


                        startActivity(intent);
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
            }
            if(isDrinks){
                myRef.child("8").addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        Food food = dataSnapshot.getValue(Food.class);

                        intent.putExtra("id",8);
                        intent.putExtra("name",food.getName());
                        intent.putExtra("desc",food.getDesc());
                        intent.putExtra("price_large",food.getPrice_large());
                        intent.putExtra("price_small",food.getPrice_small());


                        startActivity(intent);
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
            }

            if(isSlat){
                myRef.child("2").addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        Food food = dataSnapshot.getValue(Food.class);

                        intent.putExtra("id",2);
                        intent.putExtra("name",food.getName());
                        intent.putExtra("desc",food.getDesc());
                        intent.putExtra("price_large",food.getPrice_large());
                        intent.putExtra("price_small",food.getPrice_small());


                        startActivity(intent);
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
            }
        }

        else if(v.getId()==R.id.btn_item3){

            final Intent intent = new Intent(FoodActivity.this,FoodDetailActivity.class);
            Intent intent1= getIntent();
            boolean isSweet = intent1.getBooleanExtra("isSweet",false);
            boolean isSlat = intent1.getBooleanExtra("isSlat",false);
            boolean isDrinks = intent1.getBooleanExtra("isDrink",false);


            if(isSweet){

                myRef.child("6").addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        Food food = dataSnapshot.getValue(Food.class);

                        intent.putExtra("id",6);
                        intent.putExtra("name",food.getName());
                        intent.putExtra("desc",food.getDesc());
                        intent.putExtra("price_large",food.getPrice_large());
                        intent.putExtra("price_small",food.getPrice_small());


                        startActivity(intent);
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
            }
            if(isDrinks){
                myRef.child("9").addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        Food food = dataSnapshot.getValue(Food.class);

                        intent.putExtra("id",9);
                        intent.putExtra("name",food.getName());
                        intent.putExtra("desc",food.getDesc());
                        intent.putExtra("price_large",food.getPrice_large());
                        intent.putExtra("price_small",food.getPrice_small());


                        startActivity(intent);
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
            }

            if(isSlat){
                myRef.child("3").addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        Food food = dataSnapshot.getValue(Food.class);

                        intent.putExtra("id",3);
                        intent.putExtra("name",food.getName());
                        intent.putExtra("desc",food.getDesc());
                        intent.putExtra("price_large",food.getPrice_large());
                        intent.putExtra("price_small",food.getPrice_small());


                        startActivity(intent);
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
            }
        }


    }

    public void setViewforSweet(){

        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference("Food");


        iv_item1.setBackgroundResource(R.drawable.palacinka1);
        iv_item2.setBackgroundResource(R.drawable.palacinka2);
        iv_item3.setBackgroundResource(R.drawable.palacinka3);

        myRef.child("4").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Food food = dataSnapshot.getValue(Food.class);
                tv_item1_name.setText(food.getName());
                tv_item1_desc.setText(food.getDesc());
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        myRef.child("5").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Food food = dataSnapshot.getValue(Food.class);
                tv_item2_name.setText(food.getName());
                tv_item2_desc.setText(food.getDesc());
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        myRef.child("6").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Food food = dataSnapshot.getValue(Food.class);
                tv_item3_name.setText(food.getName());
                tv_item3_desc.setText(food.getDesc());
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


    }

    public void setViewforDrinks(){
        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference("Food");


        iv_item1.setBackgroundResource(R.drawable.cocktail1);
        iv_item2.setBackgroundResource(R.drawable.cocktail2);
        iv_item3.setBackgroundResource(R.drawable.cocktail3);

        myRef.child("7").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Food food = dataSnapshot.getValue(Food.class);
                tv_item1_name.setText(food.getName());
                tv_item1_desc.setText(food.getDesc());
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        myRef.child("8").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Food food = dataSnapshot.getValue(Food.class);
                tv_item2_name.setText(food.getName());
                tv_item2_desc.setText(food.getDesc());
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        myRef.child("9").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Food food = dataSnapshot.getValue(Food.class);
                tv_item3_name.setText(food.getName());
                tv_item3_desc.setText(food.getDesc());
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


    }
}
