package com.seminarski.ib140183;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.seminarski.ib140183.Common.Common;
import com.seminarski.ib140183.Model.BasketItem;
import com.seminarski.ib140183.Model.Order;

import java.net.CookieHandler;
import java.util.Random;

public class BasketActivity extends AppCompatActivity {

    TextView tv_food_name,tv_extras,tv_amount,tv_price,tv_food_name1,tv_extras1,tv_amount1,tv_price1,tv_total;
    LinearLayout ll_item1,ll_item2,ll_item3;
    Button btn_total;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_basket);

        tv_food_name = (TextView)findViewById(R.id.tv_food_name);
        tv_extras = (TextView)findViewById(R.id.tv_extras);
        tv_amount = (TextView)findViewById(R.id.tv_amount);
        tv_price = (TextView)findViewById(R.id.tv_price);
        tv_total = (TextView)findViewById(R.id.tv_total);

        ll_item1 = (LinearLayout)findViewById(R.id.ll_item1);
        ll_item2 = (LinearLayout)findViewById(R.id.ll_item2);
        ll_item3 = (LinearLayout)findViewById(R.id.ll_item3);

        tv_food_name1 = (TextView)findViewById(R.id.tv_food_name1);
        tv_extras1 = (TextView)findViewById(R.id.tv_extras1);
        tv_amount1 = (TextView)findViewById(R.id.tv_amount1);
        tv_price1 = (TextView)findViewById(R.id.tv_price1);

        btn_total = (Button)findViewById(R.id.btn_order);


        switch (Common.basketItems.size()){
            case 1:
                BasketItem b = Common.basketItems.get(0);
                tv_food_name.setText(b.getName());
                tv_extras.setText(b.getExtras());
                tv_amount.setText(b.getAmount());
                tv_price.setText(b.getPrice_large());
                ll_item2.setVisibility(View.GONE);
                ll_item3.setVisibility(View.GONE);
                tv_total.setText("Total : "+ Float.parseFloat( b.getPrice_large())*Float.parseFloat(b.getAmount()));

                break;

            case 2:
                BasketItem b1 = Common.basketItems.get(0);
                tv_food_name.setText(b1.getName());
                tv_extras.setText(b1.getExtras());
                tv_amount.setText(b1.getAmount());
                tv_price.setText(b1.getPrice_large());

                BasketItem b2 = Common.basketItems.get(1);
                tv_food_name1.setText(b2.getName());
                tv_extras1.setText(b2.getExtras());
                tv_amount1.setText(b2.getAmount());
                tv_price1.setText(b2.getPrice_large());
                ll_item3.setVisibility(View.GONE);

                Float tot = Float.parseFloat(b1.getPrice_large())* Float.parseFloat(b1.getAmount())+
                        Float.parseFloat(b2.getPrice_large())* Float.parseFloat(b2.getAmount());

                tv_total.setText("Total : "+ String.valueOf(tot));
                break;

            case 3:
                BasketItem b3 = Common.basketItems.get(0);
                tv_food_name.setText(b3.getName());
                tv_extras.setText(b3.getExtras());
                tv_amount.setText(b3.getAmount());
                tv_price.setText(b3.getPrice_large());

                BasketItem b4 = Common.basketItems.get(1);
                tv_food_name1.setText(b4.getName());
                tv_extras1.setText(b4.getExtras());
                tv_amount1.setText(b4.getAmount());
                tv_price1.setText(b4.getPrice_large());


                BasketItem b5 = Common.basketItems.get(1);
                tv_food_name1.setText(b5.getName());
                tv_extras1.setText(b5.getExtras());
                tv_amount1.setText(b5.getAmount());
                tv_price1.setText(b5.getPrice_large());

                Float t = Float.parseFloat(b3.getPrice_large())* Float.parseFloat(b3.getAmount())+
                        Float.parseFloat(b4.getPrice_large())* Float.parseFloat(b4.getAmount()) +
                        Float.parseFloat(b5.getPrice_large())* Float.parseFloat(b5.getAmount());

                tv_total.setText("Total : "+ String.valueOf(t));
                break;



        }

        btn_total.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final FirebaseDatabase database = FirebaseDatabase.getInstance();
                DatabaseReference myRef = database.getReference("Order");

                Order order = new Order(Common.userEmail,Common.basketItems,tv_total.getText().toString());


                Random r = new Random();
                int i1 = r.nextInt(10000- 13) + 13;
                myRef.child(String.valueOf(i1)).setValue(order);

                Toast.makeText(BasketActivity.this,"Order successfully send",Toast.LENGTH_SHORT).show();

                Common.basketItems.clear();
                finish();
            }
        });
    }
}
