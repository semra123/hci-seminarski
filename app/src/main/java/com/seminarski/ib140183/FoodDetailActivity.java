package com.seminarski.ib140183;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.seminarski.ib140183.Common.Common;
import com.seminarski.ib140183.Model.BasketItem;
import com.seminarski.ib140183.Model.Food;

public class FoodDetailActivity extends AppCompatActivity  implements View.OnClickListener {

    RadioButton rb_small,rb_large;
    RadioGroup rg_size;
    TextView tv_food_name,tv_desc,tv_select_extra;
    ImageView iv_food_image;
    Boolean is_small=true;
    String extras="";
    LinearLayout ll_extra;
    EditText et_amount;
    Button add_to_basket;
    String p1;
    String p2;

    String price;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_food_detail);

        rb_small = (RadioButton)findViewById(R.id.rb_small);
        rb_large = (RadioButton)findViewById(R.id.rb_large);
        rg_size = (RadioGroup)findViewById(R.id.rg_size);
        iv_food_image = (ImageView)findViewById(R.id.iv_food_image);
        tv_food_name = (TextView)findViewById(R.id.food_name);
        tv_select_extra = (TextView)findViewById(R.id.tv_select_extra);
        et_amount = (EditText)findViewById(R.id.et_amount);
        tv_desc = (TextView)findViewById(R.id.tv_desc);
        tv_select_extra.setOnClickListener(this);
        ll_extra = (LinearLayout)findViewById(R.id.ll_extra);

        add_to_basket = (Button)findViewById(R.id.btn_add_to_basket);
        add_to_basket.setOnClickListener(this);

        Intent intent = getIntent();
        final int id = intent.getIntExtra("id",1);
        String name = intent.getStringExtra("name");
        String desc = intent.getStringExtra("desc");
        String price_large = intent.getStringExtra("price_large");
        String price_small = intent.getStringExtra("price_small");




        switch (id) {

            case 1 :
                iv_food_image.setBackgroundResource(R.drawable.margherita);
                tv_food_name.setText(name);
                tv_desc.setText(desc);
                rb_large.setText("Large "+price_large+" $");
                rb_small.setText("Small "+price_small+" $");

                break;

            case 2 :
                iv_food_image.setBackgroundResource(R.drawable.napoli);
                tv_food_name.setText(name);
                tv_desc.setText(desc);
                rb_large.setText("Large "+price_large+" $");
                rb_small.setText("Small "+price_small+" $");

                break;


            case 3 :
                iv_food_image.setBackgroundResource(R.drawable.quattro);
                tv_food_name.setText(name);
                tv_desc.setText(desc);
                rb_large.setText("Large "+price_large+" $");
                rb_small.setText("Small "+price_small+" $");

                break;


            case 4 :
                iv_food_image.setBackgroundResource(R.drawable.palacinka1);
                tv_food_name.setText(name);
                tv_desc.setText(desc);
                rb_large.setText("Large "+price_large+" $");
                rb_small.setVisibility(View.GONE);

                break;

            case 5 :
                iv_food_image.setBackgroundResource(R.drawable.palacinka2);
                tv_food_name.setText(name);
                tv_desc.setText(desc);
                rb_large.setText("Large "+price_large+" $");
                rb_small.setVisibility(View.GONE);

                break;

            case 6 :
                iv_food_image.setBackgroundResource(R.drawable.palacinka3);
                tv_food_name.setText(name);
                tv_desc.setText(desc);
                rb_large.setText("Large "+price_large+" $");
                rb_small.setVisibility(View.GONE);

                break;

            case 7 :
                iv_food_image.setBackgroundResource(R.drawable.cocktail1);
                tv_food_name.setText(name);
                tv_desc.setText(desc);
                rb_large.setText("Large "+price_large+" $");
                rb_small.setVisibility(View.GONE);
                ll_extra.setVisibility(View.GONE);

                break;

            case 8 :
                iv_food_image.setBackgroundResource(R.drawable.cocktail2);
                tv_food_name.setText(name);
                tv_desc.setText(desc);
                rb_large.setText("Large "+price_large+" $");
                rb_small.setVisibility(View.GONE);
                ll_extra.setVisibility(View.GONE);

                break;

            case 9 :
                iv_food_image.setBackgroundResource(R.drawable.cocktail3);
                tv_food_name.setText(name);
                tv_desc.setText(desc);
                rb_large.setText("Large "+price_large+" $");
                rb_small.setVisibility(View.GONE);
                ll_extra.setVisibility(View.GONE);

                break;


        }




        rg_size.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener(){
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                switch(checkedId)
                {
                    case R.id.rb_small:
                        Intent intent1 = getIntent();
                        is_small=true;
                       p1 = intent1.getStringExtra("price_small");
                        break;
                    case R.id.rb_large:
                        Intent intent2 = getIntent();
                        p1 = intent2.getStringExtra("price_large");
                        is_small=false;
                        break;
                }
            }
        });


    }

    @Override
    public void onClick(View v) {
        if(v.getId()==R.id.tv_select_extra){
            showAlertDialog();
        }

        else if(v.getId()==R.id.btn_add_to_basket){
            String price1 = rb_large.getText().toString();
            String price2 = rb_small.getText().toString();
            if(!is_small)
                price1=price2;



            if(price1==null || et_amount.getText().toString().equalsIgnoreCase("0")||et_amount.getText().toString().isEmpty() ||(p1==null && p2==null ) ){
                Toast.makeText(FoodDetailActivity.this,"Please first fill empty fields",Toast.LENGTH_SHORT).show();
            }

            else{
                BasketItem basketItem = new BasketItem(tv_food_name.getText().toString(),p1,p2,et_amount.getText().toString(),extras);
                Common.basketItems.add(basketItem);
                Toast.makeText(FoodDetailActivity.this,"You successfully added this item to basket",Toast.LENGTH_SHORT).show();
            }


        }
    }

    private void showAlertDialog() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(FoodDetailActivity.this);
        alertDialog.setTitle("Select your extras");
        alertDialog.setMessage("Select your extras:");

        LayoutInflater inflater = this.getLayoutInflater();
        View order_extras = inflater.inflate(R.layout.order_extras, null);

        final CheckBox c1 = (CheckBox)order_extras.findViewById(R.id.checkbox_1);
        final CheckBox c2 = (CheckBox)order_extras.findViewById(R.id.checkbox_2);

        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference("Extras");

        if(getIntent().getIntExtra("id",1) == 1 || getIntent().getIntExtra("id",1) == 2 || getIntent().getIntExtra("id",1) == 3){
            myRef.child("1").child("name").addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    extras = dataSnapshot.getValue(String.class);
                    c1.setText(extras);
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });


        myRef.child("2").child("name").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                 extras = dataSnapshot.getValue(String.class);
                c2.setText(extras);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });}
        if(getIntent().getIntExtra("id",1) == 4 || getIntent().getIntExtra("id",1) == 5 || getIntent().getIntExtra("id",1) == 6){
            myRef.child("3").child("name").addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                     extras = dataSnapshot.getValue(String.class);
                    c1.setText(extras);
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });


            myRef.child("4").child("name").addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    extras = dataSnapshot.getValue(String.class);
                    c2.setText(extras);
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });}






        alertDialog.setView(order_extras);
        alertDialog.setIcon(R.drawable.ic_check_black_24dp);

        alertDialog.setPositiveButton("CONFIRM", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if(c1.isChecked() && c2.isChecked()){
                    extras = c1.getText().toString() +" , "+ c2.getText().toString();
                }
                else if(c1.isChecked() && !c2.isChecked()){
                    extras = c1.getText().toString();
                }
                else if(!c1.isChecked() && c2.isChecked()){
                    extras=c2.getText().toString();
                }
            }
        });

        alertDialog.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        alertDialog.show();

    }
}
