package com.seminarski.ib140183;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.seminarski.ib140183.Model.User;

public class EditProfilActivity extends AppCompatActivity {

    EditText et_first_name_edit,et_last_name_edit,et_username_edit,et_address_edit,et_phone_edit;
    Button btn_update;
    String email;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profil);

        et_first_name_edit = (EditText)findViewById(R.id.et_first_name_edit);
        et_last_name_edit = (EditText)findViewById(R.id.et_last_name_edit);
        et_username_edit = (EditText)findViewById(R.id.et_username_edit);
        et_address_edit = (EditText)findViewById(R.id.et_address_edi);
        et_phone_edit= (EditText)findViewById(R.id.et_telephone_edit);

        btn_update = (Button)findViewById(R.id.btn_update);



        FirebaseUser user1 = FirebaseAuth.getInstance().getCurrentUser();
        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference("User");

        myRef.child(user1.getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                User user = dataSnapshot.getValue(User.class);

                et_first_name_edit.setText(user.getFirstName());
                et_last_name_edit.setText(user.getLastName());
                et_username_edit.setText(user.getUsername());
                et_address_edit.setText(user.getAddress());
                et_phone_edit.setText(user.getTelephone());
                email = user.getEmail();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


        btn_update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final FirebaseDatabase database = FirebaseDatabase.getInstance();
                DatabaseReference myRef = database.getReference("User");
                FirebaseUser user1 = FirebaseAuth.getInstance().getCurrentUser();

                User user = new User(et_first_name_edit.getText().toString(),et_last_name_edit.getText().toString(),
                        et_username_edit.getText().toString(),email,et_address_edit.getText().toString(),
                        et_phone_edit.getText().toString());

                myRef.child(user1.getUid()).setValue(user);
                Toast.makeText(EditProfilActivity.this,"Your profile is update successfully!",Toast.LENGTH_SHORT).show();


            }
        });

    }
}
