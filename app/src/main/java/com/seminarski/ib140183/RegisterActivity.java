package com.seminarski.ib140183;


import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.seminarski.ib140183.Model.User;

public class RegisterActivity extends AppCompatActivity {

    EditText et_email,et_password,etFirstName,etLastName,etUsername,etAddress,etTelephone;
    private FirebaseAuth mAuth;
    Button btn_signup;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        et_email = (EditText)findViewById(R.id.et_email);
        et_password = (EditText)findViewById(R.id.et_password_reg);
        etFirstName = (EditText)findViewById(R.id.et_first_name);
        etLastName = (EditText)findViewById(R.id.et_last_name);
        etUsername= (EditText)findViewById(R.id.et_username);
        etAddress = (EditText)findViewById(R.id.et_address);
        etTelephone = (EditText)findViewById(R.id.et_telephone);

        btn_signup = (Button)findViewById(R.id.btn_signup);
        mAuth = FirebaseAuth.getInstance();
        final String email = et_email.getText().toString();
        final String password = et_password.getText().toString();

        btn_signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (et_email.getText().toString().isEmpty() || et_password.getText().toString().isEmpty() || etFirstName.getText().toString().isEmpty()
                        || etLastName.getText().toString().isEmpty() || etUsername.getText().toString().isEmpty() || etUsername.getText().toString().isEmpty()
                        || etAddress.getText().toString().isEmpty() || etTelephone.getText().toString().isEmpty()) {

                    Toast.makeText(RegisterActivity.this, "All fields must be filled", Toast.LENGTH_SHORT).show();
                } else {

                    mAuth.createUserWithEmailAndPassword(et_email.getText().toString(), et_password.getText().toString())
                            .addOnCompleteListener(RegisterActivity.this, new OnCompleteListener<AuthResult>() {
                                @Override
                                public void onComplete(@NonNull Task<AuthResult> task) {
                                    if (task.isSuccessful()) {
                                        // Sign in success, update UI with the signed-in user's information
                                        FirebaseUser user = mAuth.getCurrentUser();

                                        final FirebaseDatabase database = FirebaseDatabase.getInstance();
                                        DatabaseReference myRef = database.getReference("User");

                                        User user1 = new User(etFirstName.getText().toString(),etLastName.getText().toString(),etUsername.getText().toString(),et_email.getText().toString(),etAddress.getText().toString(),etTelephone.getText().toString());

                                        myRef.child(user.getUid()).setValue(user1);

                                        finish();
                                    } else {
                                        // If sign in fails, display a message to the user.

                                        Toast.makeText(RegisterActivity.this, "Authentication failed.",
                                                Toast.LENGTH_SHORT).show();
                                    }

                                }
                            });

                    FirebaseUser user1 = FirebaseAuth.getInstance().getCurrentUser();



                }
            }
        });




    }
}
