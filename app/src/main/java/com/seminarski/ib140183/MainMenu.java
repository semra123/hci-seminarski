package com.seminarski.ib140183;

import android.content.Intent;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import android.view.View;

import androidx.core.view.GravityCompat;
import androidx.appcompat.app.ActionBarDrawerToggle;

import android.view.MenuItem;

import com.google.android.material.navigation.NavigationView;
import com.seminarski.ib140183.Common.Common;
import com.seminarski.ib140183.Model.Reservation;

import androidx.drawerlayout.widget.DrawerLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.Menu;
import android.widget.Button;
import android.widget.TextView;

public class MainMenu extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener,View.OnClickListener {

    Button btn_food_explore,btn_sweet_explore,btn_drink_explore;
    TextView tv_user_name,tv_email;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        btn_food_explore = (Button)findViewById(R.id.btn_food_explore);
        btn_sweet_explore = (Button)findViewById(R.id.btn_sweets_explore);
        btn_drink_explore = (Button)findViewById(R.id.btn_drinks_explore);
        tv_user_name = (TextView)findViewById(R.id.tv_user_name);

        btn_food_explore.setOnClickListener(this);
        btn_sweet_explore.setOnClickListener(this);
        btn_drink_explore.setOnClickListener(this);
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);




    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(android.view.Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);


        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_menu) {
            // Handle the camera action
        } else if (id == R.id.nav_basket) {
            Intent intent = new Intent(MainMenu.this,BasketActivity.class);
            startActivity(intent);

        } else if (id == R.id.nav_account) {
            Intent intent = new Intent(MainMenu.this, EditProfilActivity.class);
            startActivity(intent);

        } else if (id == R.id.reservation) {
            Intent intent = new Intent(MainMenu.this, ReservationActivity.class);
            startActivity(intent);

        } else if (id == R.id.nav_info) {
            Intent intent = new Intent(MainMenu.this, ContactActivity.class);
            startActivity(intent);

        } else if (id == R.id.nav_logout) {
            finish();

        }



        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);

        //tv_email.setText(Common.userEmail);


        return true;
    }

    @Override
    public void onClick(View v) {
        if(v.getId()==R.id.btn_food_explore){

            Intent intent = new Intent(MainMenu.this,FoodActivity.class);
            intent.putExtra("isSlat",true);
            startActivity(intent);
        }

       else if(v.getId()==R.id.btn_sweets_explore){
            Intent intent = new Intent(MainMenu.this,FoodActivity.class);
            intent.putExtra("isSweet",true);
            startActivity(intent);
        }

        else if(v.getId()==R.id.btn_drinks_explore){
            Intent intent = new Intent(MainMenu.this,FoodActivity.class);
            intent.putExtra("isDrink",true);
            startActivity(intent);
        }
    }
}
