package com.seminarski.ib140183;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.seminarski.ib140183.Common.Common;
import com.seminarski.ib140183.Model.Reservation;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class ReservationActivity extends AppCompatActivity {

    EditText et_date,et_time,et_person_number;
    Button btn_make_reservation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reservation);

        et_date = (EditText)findViewById(R.id.et_date);
        et_time = (EditText)findViewById(R.id.et_time);
        et_person_number = (EditText)findViewById(R.id.et_person_number);
        btn_make_reservation = (Button)findViewById(R.id.btn_make_reservation);


        final Calendar myCalendar = Calendar.getInstance();

        btn_make_reservation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(et_date.getText().toString().isEmpty()|| et_time.getText().toString().isEmpty() || et_person_number.getText().toString().isEmpty()){
                    Toast.makeText(ReservationActivity.this,"Some fields are empty",Toast.LENGTH_SHORT).show();
                }

                else{
                    Reservation reservation = new Reservation(Common.userEmail,et_date.getText().toString(),et_time.getText().toString(),et_person_number.getText().toString());
                    final FirebaseDatabase database = FirebaseDatabase.getInstance();
                    DatabaseReference myRef = database.getReference("Reservation");

                    String reservationID = et_date.getText().toString().substring(0,2) + et_time.getText().toString().substring(0,2)+et_person_number.getText().toString().substring(0,1);


                    myRef.child(reservationID).setValue(reservation);
                    Toast.makeText(ReservationActivity.this,"Reservation successfully send",Toast.LENGTH_SHORT).show();
                }
            }
        });

        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);


                String myFormat = "dd/MM/yy"; //In which you need put here
                SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.getDefault());

                et_date.setText(sdf.format(myCalendar.getTime()));
            }

        };

        et_date.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                new DatePickerDialog(ReservationActivity.this, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });
    }


}
