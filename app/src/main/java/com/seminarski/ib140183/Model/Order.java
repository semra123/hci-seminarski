package com.seminarski.ib140183.Model;

import java.util.ArrayList;

public class Order {
    private String username;
    private String address;
    private ArrayList<BasketItem> basketItems;
    private String total;


    public Order() {
    }

    public Order(String username, ArrayList<BasketItem> basketItems, String total) {
        this.username = username;
        this.basketItems = basketItems;
        this.total = total;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public ArrayList<BasketItem> getBasketItems() {
        return basketItems;
    }

    public void setBasketItems(ArrayList<BasketItem> basketItems) {
        this.basketItems = basketItems;
    }
}
