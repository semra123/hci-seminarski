package com.seminarski.ib140183.Model;

public class Reservation {
   private String name;
   private String date;
   private String time;
   private String person_number;


    public Reservation() {
    }

    public Reservation(String name, String date, String time, String person_number) {
        this.name = name;
        this.date = date;
        this.time = time;
        this.person_number = person_number;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getPerson_number() {
        return person_number;
    }

    public void setPerson_number(String person_number) {
        this.person_number = person_number;
    }
}
