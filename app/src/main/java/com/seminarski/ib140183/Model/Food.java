package com.seminarski.ib140183.Model;

public class Food {

    private String desc;
    private String name;
    private String price_large;
    private String price_small;

    public Food() {
    }

    public Food(String desc, String name, String price_large, String price_small) {
        this.desc = desc;
        this.name = name;
        this.price_large = price_large;
        this.price_small = price_small;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrice_large() {
        return price_large;
    }

    public void setPrice_large(String price_large) {
        this.price_large = price_large;
    }

    public String getPrice_small() {
        return price_small;
    }

    public void setPrice_small(String price_small) {
        this.price_small = price_small;
    }
}
