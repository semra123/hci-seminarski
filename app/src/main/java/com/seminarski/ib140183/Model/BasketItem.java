package com.seminarski.ib140183.Model;

public class BasketItem {

    private String name;
    private String price_large;
    private String price_small;
    private String amount;
    private String extras;


    public BasketItem() {
    }

    public BasketItem(String name, String price_large, String price_small, String amount, String extras) {
        this.name = name;
        this.price_large = price_large;
        this.price_small = price_small;
        this.amount = amount;
        this.extras = extras;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrice_large() {
        return price_large;
    }

    public void setPrice_large(String price_large) {
        this.price_large = price_large;
    }

    public String getPrice_small() {
        return price_small;
    }

    public void setPrice_small(String price_small) {
        this.price_small = price_small;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getExtras() {
        return extras;
    }

    public void setExtras(String extras) {
        this.extras = extras;
    }
}
